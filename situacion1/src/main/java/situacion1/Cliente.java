package situacion1;

import java.math.BigDecimal;
import java.util.ArrayList;

public abstract class Cliente {
    private Integer codigo;
    private String apellido;
    private String n_cuenta_b;
    private String direccion;
    private String telefono;
    private ArrayList<Persona> personas;
    private ArrayList<Mascota> mascotas;
    private String email ;
    private String whatsapp ;
    
    //Constructor

    public Cliente (Integer codigo, String apellido, String n_cuenta_b, String direccion, String telefono, ArrayList<Persona> personas, ArrayList<Mascota> mascotas, String email, String whatsapp){
        this.codigo = codigo ;
        this.apellido = apellido ;
        this.n_cuenta_b = n_cuenta_b ;
        this.direccion = direccion ;
        this.telefono = telefono ;
        this.personas = personas ;
        this.mascotas = mascotas ;
        this.email = email ;
        this.whatsapp = whatsapp ;
    }

    /*public void agregarPersona(Persona persona){
        personas.add(persona);
    }*/

    //Metodos getters y setters
    
    public Integer getCodigo() {
        return codigo;
    }
    
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
    
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public String getN_cuenta_b() {
        return n_cuenta_b;
    }
    
    public void setN_cuenta_b(String n_cuenta_b) {
        this.n_cuenta_b = n_cuenta_b;
    }
    
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public ArrayList<Persona> getPersonas() {
        return personas;
    }
    
    public void setPersonas(ArrayList<Persona> personas) {
        this.personas = personas;
    }
    
    public ArrayList<Mascota> getMascotas() {
        return mascotas;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public void setMascotas(ArrayList<Mascota> mascotas) {
        this.mascotas = mascotas;
    }

    public abstract BigDecimal getBonificacion();
    


}