package situacion1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class ClienteVip extends Cliente {

    public ClienteVip(Integer codigo, String apellido, String n_cuenta_b, String direccion, String telefono, ArrayList<Persona> personas, ArrayList<Mascota> mascotas, String email, String whatsapp) {
        super(codigo, apellido, n_cuenta_b, direccion, telefono, personas, mascotas, email, whatsapp);
    }

    @Override
    public BigDecimal getBonificacion() {
        return new BigDecimal(0.10).setScale(2, RoundingMode.HALF_EVEN);
    }
}