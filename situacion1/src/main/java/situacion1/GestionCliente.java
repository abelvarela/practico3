package situacion1;

import java.util.ArrayList;

public class GestionCliente {

    private ArrayList<Cliente> clientes;


    public GestionCliente (){
        clientes = new ArrayList<Cliente>();
    }


    
    public void agregarCliente(Integer codigo, String apellido, String n_cuenta_b, String direccion, String telefono, ArrayList<Persona> personas, ArrayList<Mascota> mascotas, String email, String whatsapp){
        if(mascotas.size()>2)
            clientes.add(new ClienteVip(codigo, apellido, n_cuenta_b, direccion, telefono, personas, mascotas, email, whatsapp));
        else
            clientes.add(new ClienteComun(codigo, apellido, n_cuenta_b, direccion, telefono, personas, mascotas, email, whatsapp));
    }




    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }








}