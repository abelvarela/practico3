package situacion1;

public class HClinica {
    private String enfermedad;
    private String fecha;


    //Constructor

    public HClinica (String enfermedad, String fecha){
        this.enfermedad = enfermedad ;
        this.fecha = fecha ;
    }


    //Metodos getters y setters

    public String getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(String enfermedad) {
        this.enfermedad = enfermedad;
    }
    
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}