package situacion1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Mascota {
    private Integer codigo;
    private String alias;
    private String especie;
    private String raza;
    private String color_pelo;
    private String fec_nac;
    //private Float peso_actual;
    private ArrayList<BigDecimal> pesos;
    private ArrayList<HClinica> h_clinica;
    private ArrayList<CVacun> c_vacun;


    //Constructor

    public Mascota (Integer codigo, String alias, String especie, String raza, String color_pelo, String fec_nac){
        this.codigo = codigo ;
        this.alias = alias ;
        this.especie = especie ;
        this.raza = raza ;
        this.color_pelo = color_pelo ;
        this.fec_nac = fec_nac ;
        //this.peso_actual = peso_actual ;
        pesos = new ArrayList<BigDecimal>() ;
        h_clinica = new ArrayList<HClinica>() ;
        c_vacun = new ArrayList<CVacun>() ;
    }



    public BigDecimal ppromedio (){
        if (pesos.isEmpty()) return new BigDecimal(0.00).setScale(2, RoundingMode.HALF_EVEN) ;
        
        BigDecimal total = new BigDecimal(0.00) ;

        for (BigDecimal var : pesos) {
            total = total.add(var) ;
        }
        
        return total.divide(new BigDecimal(pesos.size()),2,RoundingMode.HALF_EVEN);
    }
    
    public BigDecimal getPeso_actual() {
        return pesos.get(pesos.size()-1).setScale(2, RoundingMode.HALF_EVEN);
    }

    public void setPeso_actual(BigDecimal peso_actual) {
        pesos.add(peso_actual);
    }

    


    //Metodos getters y setters
    
    public Integer getCodigo() {
        return codigo;
    }
    
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
    
    public String getAlias() {
        return alias;
    }
    
    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getRaza() {
        return raza;
    }
    
    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getColor_pelo() {
        return color_pelo;
    }

    public void setColor_pelo(String color_pelo) {
        this.color_pelo = color_pelo;
    }

    public String getFec_nac() {
        return fec_nac;
    }
    
    public void setFec_nac(String fec_nac) {
        this.fec_nac = fec_nac;
    }
    
    public ArrayList<BigDecimal> getPesos() {
        return pesos;
    }

    public void setPesos(ArrayList<BigDecimal> pesos) {
        this.pesos = pesos;
    }
    
    public ArrayList<HClinica> getH_clinica() {
        return h_clinica;
    }
    
    public void setH_clinica(ArrayList<HClinica> h_clinica) {
        this.h_clinica = h_clinica;
    }
    
    public ArrayList<CVacun> getC_vacun() {
        return c_vacun;
    }

    public void setC_vacun(ArrayList<CVacun> c_vacun) {
        this.c_vacun = c_vacun;
    }


    
}