package situacion1;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;

public class GestionClienteTest {
    
    @Test
    public void tipoCorrectoConUnaMascota(){
        ArrayList<Mascota> ms = new ArrayList<Mascota>();
        ms.add(new Mascota(1, "alias", "especie", "raza", "color_pelo", "fec_nac"));

        ArrayList<Persona> ps = new ArrayList<Persona>();
        ps.add(new Persona("Abel", "Varela", "34233"));
        ps.add(new Persona("Juan", "Perez", "56533"));
        ps.add(new Persona("Joaquin", "Barrionuevo", "36523"));
        ps.add(new Persona("Marcos", "Toloza", "98233"));

        GestionCliente gc = new GestionCliente();

        gc.agregarCliente(1, "apellido", "n_cuenta_b", "direccion", "telefono", ps, ms, "email", "whatsapp");

        
        assertEquals(true, gc.getClientes().get(0) instanceof ClienteComun);
    }

    @Test
    public void tipoCorrectoConDosMascotas(){
        ArrayList<Mascota> ms = new ArrayList<Mascota>();
        ms.add(new Mascota(1, "alias", "especie", "raza", "color_pelo", "fec_nac"));
        ms.add(new Mascota(2, "alias", "especie", "raza", "color_pelo", "fec_nac"));

        ArrayList<Persona> ps = new ArrayList<Persona>();
        ps.add(new Persona("Abel", "Varela", "34233"));
        ps.add(new Persona("Juan", "Perez", "56533"));
        ps.add(new Persona("Joaquin", "Barrionuevo", "36523"));
        ps.add(new Persona("Marcos", "Toloza", "98233"));

        GestionCliente gc = new GestionCliente();

        gc.agregarCliente(1, "apellido", "n_cuenta_b", "direccion", "telefono", ps, ms, "email", "whatsapp");

        
        assertEquals(true, gc.getClientes().get(0) instanceof ClienteComun);
    }

    @Test
    public void tipoCorrectoConTresMascotas(){
        ArrayList<Mascota> ms = new ArrayList<Mascota>();
        ms.add(new Mascota(1, "alias", "especie", "raza", "color_pelo", "fec_nac"));
        ms.add(new Mascota(2, "alias", "especie", "raza", "color_pelo", "fec_nac"));
        ms.add(new Mascota(3, "alias", "especie", "raza", "color_pelo", "fec_nac"));

        ArrayList<Persona> ps = new ArrayList<Persona>();
        ps.add(new Persona("Abel", "Varela", "34233"));
        ps.add(new Persona("Juan", "Perez", "56533"));
        ps.add(new Persona("Joaquin", "Barrionuevo", "36523"));
        ps.add(new Persona("Marcos", "Toloza", "98233"));

        GestionCliente gc = new GestionCliente();

        gc.agregarCliente(1, "apellido", "n_cuenta_b", "direccion", "telefono", ps, ms, "email", "whatsapp");

        
        assertEquals(true, gc.getClientes().get(0) instanceof ClienteVip);
    }

    

}