package situacion1;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class MascotaTest {
    
    @Test
    public void promedioArrayVacio(){
        Mascota mascota = new Mascota(1, "alias", "especie", "raza", "color_pelo", "fec_nac") ;

        assertEquals(mascota.ppromedio(), new BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void promedioArrayMitad(){
        Mascota mascota = new Mascota(1, "alias", "especie", "raza", "color_pelo", "fec_nac") ;

        mascota.setPeso_actual(new BigDecimal(5));
        mascota.setPeso_actual(new BigDecimal(6));
        mascota.setPeso_actual(new BigDecimal(7));
        mascota.setPeso_actual(new BigDecimal(8));
        mascota.setPeso_actual(new BigDecimal(9));

        assertEquals(mascota.ppromedio(), new BigDecimal(7).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void promedioArrayLLeno(){
        Mascota mascota = new Mascota(1, "alias", "especie", "raza", "color_pelo", "fec_nac") ;

        mascota.setPeso_actual(new BigDecimal(5));
        mascota.setPeso_actual(new BigDecimal(6));
        mascota.setPeso_actual(new BigDecimal(7));
        mascota.setPeso_actual(new BigDecimal(8));
        mascota.setPeso_actual(new BigDecimal(9));

        mascota.setPeso_actual(new BigDecimal(8));
        mascota.setPeso_actual(new BigDecimal(8));
        mascota.setPeso_actual(new BigDecimal(8));
        mascota.setPeso_actual(new BigDecimal(8));
        mascota.setPeso_actual(new BigDecimal(8));

        assertEquals(mascota.ppromedio(), new BigDecimal(7.5).setScale(2, RoundingMode.HALF_EVEN));
    }


}