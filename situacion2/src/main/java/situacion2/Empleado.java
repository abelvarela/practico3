package situacion2;

import java.math.BigDecimal;

public class Empleado {
    private String tipo_documento;
    private String numero_documento;
    private String apellido;
    private String nombres;
    private String domicilio;
    private BigDecimal sueldo_basico;
    private BigDecimal sueldo_total;

    
    //Constructor
    public Empleado (String tipo_documento, String numero_documento, String apellido, String nombres, String domicilio, BigDecimal sueldo_basico){
        this.tipo_documento = tipo_documento ;
        this.numero_documento = numero_documento ;
        this.apellido = apellido ;
        this.nombres = nombres ;
        this.domicilio = domicilio ;
        this.sueldo_basico = sueldo_basico ;
    }



    public BigDecimal calcular_sueldo() {
		return sueldo_basico;
    }



    //Métodos getters y setters
    public String getTipo_documento() {
        return tipo_documento;
    }
    
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    public String getNumero_documento() {
        return numero_documento;
    }
    
    public void setNumero_documento(String numero_documento) {
        this.numero_documento = numero_documento;
    }
    
    public String getApellido() {
        return apellido;
    }
    
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public String getNombres() {
        return nombres;
    }
    
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    
    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    
    public BigDecimal getSueldo_basico() {
        return sueldo_basico;
    }
    
    public void setSueldo_basico(BigDecimal sueldo_basico) {
        this.sueldo_basico = sueldo_basico;
    }
    
    public BigDecimal getSueldo_total() {
        return sueldo_total;
    }

    public void setSueldo_total(BigDecimal sueldo_total) {
        this.sueldo_total = sueldo_total;
    }





}