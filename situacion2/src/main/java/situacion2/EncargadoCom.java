package situacion2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class EncargadoCom extends Empleado {
    
	private ArrayList <Vendedor> vendedores;


    //Constructor
    public EncargadoCom(String tipo_documento, String numero_documento, String apellido, String nombres, String domicilio, BigDecimal sueldo_basico){
        super(tipo_documento, numero_documento, apellido, nombres, domicilio, sueldo_basico);
        this.vendedores = new ArrayList<Vendedor>() ;
    }



    public BigDecimal calcular_sueldo() {
        
        Integer total = new Integer(0);
        for (Vendedor v : this.vendedores) {
            total += v.getCant_contratos_nuevos() ;
        }
        
        BigDecimal t = new BigDecimal(total);

        setSueldo_total(getSueldo_basico().add(getSueldo_basico().multiply(new BigDecimal(0.01).multiply(t))).setScale(2, RoundingMode.HALF_EVEN));

        if (getSueldo_total().compareTo(new BigDecimal(55000)) == 1)
            setSueldo_total(getSueldo_total().multiply(new BigDecimal(0.95)));


        for (Vendedor v : this.vendedores) {
            v.setCant_contratos_nuevos(0) ;
        }

        return getSueldo_total().setScale(2, RoundingMode.HALF_EVEN) ;
    }



    //Métodos getters y setters
    public ArrayList<Vendedor> getVendedores() {
        return vendedores;
    }

    public void setVendedores(ArrayList<Vendedor> vendedores) {
        this.vendedores = vendedores;
    }


}