package situacion2;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Limpiador extends Empleado {
    
    private Integer cant_hextra;
    
    
    //Constructor
    public Limpiador(String tipo_documento, String numero_documento, String apellido, String nombres, String domicilio, BigDecimal sueldo_basico, Integer cant_hextra) {
		super(tipo_documento, numero_documento, apellido, nombres, domicilio, sueldo_basico);
	    this.cant_hextra = cant_hextra ;
	}
    


    public BigDecimal calcular_sueldo() {
                
        BigDecimal che = new BigDecimal(cant_hextra);
        

        setSueldo_total(getSueldo_basico().add(getSueldo_basico().multiply(new BigDecimal(0.005).multiply(che)).setScale(2, RoundingMode.HALF_EVEN)));

        if (getSueldo_total().compareTo(new BigDecimal(55000)) == 1)
            setSueldo_total(getSueldo_total().multiply(new BigDecimal(0.95).setScale(2, RoundingMode.HALF_EVEN)));

        return getSueldo_total().setScale(2, RoundingMode.HALF_EVEN) ;
    }



    //Métodos getters y setters
    public Integer getCant_hextra() {
        return cant_hextra;
    }

    public void setCant_hextra(Integer cant_hextra) {
        this.cant_hextra = cant_hextra;
    }


}