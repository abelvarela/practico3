package situacion2;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PersonalAdm extends Empleado {

    
    //Constructor
    public PersonalAdm(String tipo_documento, String numero_documento, String apellido, String nombres, String domicilio, BigDecimal sueldo_basico) {
		super(tipo_documento, numero_documento, apellido, nombres, domicilio, sueldo_basico);
	}

    
    public BigDecimal calcular_sueldo() {
        if (getSueldo_basico().compareTo(new BigDecimal(55000)) == 1)
            return getSueldo_basico().multiply(new BigDecimal(0.95)).setScale(2, RoundingMode.HALF_EVEN);
        
        return getSueldo_basico().setScale(2, RoundingMode.HALF_EVEN);
    }


}