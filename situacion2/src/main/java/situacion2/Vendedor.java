package situacion2;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Vendedor extends Empleado {
    private Integer cant_contratos;
    private Integer cant_contratos_nuevos;

    

    //Constructor
    public Vendedor(String tipo_documento, String numero_documento, String apellido, String nombres, String domicilio, BigDecimal sueldo_basico, Integer cant_contratos, Integer cant_contratos_nuevos){
        super(tipo_documento, numero_documento, apellido, nombres, domicilio, sueldo_basico);
        this.cant_contratos = cant_contratos ;
        this.cant_contratos_nuevos = cant_contratos_nuevos ;
    }    
    
    

    public BigDecimal calcular_sueldo() {        
        BigDecimal cc = new BigDecimal (cant_contratos);
        if (cant_contratos < 10){
            setSueldo_total(getSueldo_basico().add(getSueldo_basico().multiply(new BigDecimal(0.01).multiply(cc))).setScale(2, RoundingMode.HALF_EVEN));
        }else{
            setSueldo_total(getSueldo_basico().add(getSueldo_basico().multiply(new BigDecimal(0.015).multiply(cc))).setScale(2, RoundingMode.HALF_EVEN));
        }

        if (getSueldo_total().compareTo(new BigDecimal(55000)) == 1)
            setSueldo_total(getSueldo_total().multiply(new BigDecimal(0.95).setScale(2, RoundingMode.HALF_EVEN)));

        return getSueldo_total().setScale(2, RoundingMode.HALF_EVEN) ;
    }


    
    //Métodos getters y setters
    public Integer getCant_contratos() {
        return cant_contratos;
    }

    public void setCant_contratos(Integer cant_contratos) {
        this.cant_contratos = cant_contratos;
    }

    public Integer getCant_contratos_nuevos() {
        return cant_contratos_nuevos;
    }

    public void setCant_contratos_nuevos(Integer cant_contratos_nuevos) {
        this.cant_contratos_nuevos = cant_contratos_nuevos;
    }









}