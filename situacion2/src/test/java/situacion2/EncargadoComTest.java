package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.math.RoundingMode;



public class EncargadoComTest{
    @Test
    public void calcularSueldoSinContratos(){
        Vendedor v1 = new Vendedor("dni","37632213","Varela","Abel Alejandro","Polcos",new BigDecimal(8),9,0);
        Vendedor v2 = new Vendedor("dni","80632213","Lopez","Joaquin Marcos","SantaRosa",new BigDecimal(100),20,0);
        
        EncargadoCom ec = new EncargadoCom("dni", "25177544", "Favore", "Veronica Claudia", "Casa93", new BigDecimal(100));
        ec.getVendedores().add(v1);
        ec.getVendedores().add(v2);
        
        
        assertEquals(ec.calcular_sueldo(), new BigDecimal(100).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void calcularSueldoConContratos(){
        Vendedor v1 = new Vendedor("dni","37632213","Varela","Abel Alejandro","Polcos",new BigDecimal(8),9,5);
        Vendedor v2 = new Vendedor("dni","80632213","Lopez","Joaquin Marcos","SantaRosa",new BigDecimal(100),20,9);
        
        EncargadoCom ec = new EncargadoCom("dni", "25177544", "Favore", "Veronica Claudia", "Casa93", new BigDecimal(100));
        ec.getVendedores().add(v1);
        ec.getVendedores().add(v2);
        
        
        assertEquals(ec.calcular_sueldo(), new BigDecimal(114).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void calcularSueldoSinContratosMayorA55(){
        Vendedor v1 = new Vendedor("dni","37632213","Varela","Abel Alejandro","Polcos",new BigDecimal(800),9,0);
        Vendedor v2 = new Vendedor("dni","80632213","Lopez","Joaquin Marcos","SantaRosa",new BigDecimal(100),20,0);
        
        EncargadoCom ec = new EncargadoCom("dni", "25177544", "Favore", "Veronica Claudia", "Casa93", new BigDecimal(56000));
        ec.getVendedores().add(v1);
        ec.getVendedores().add(v2);
        
        
        assertEquals(ec.calcular_sueldo(), new BigDecimal(53200).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void calcularSueldoConContratosMayorA55(){
        Vendedor v1 = new Vendedor("dni","37632213","Varela","Abel Alejandro","Polcos",new BigDecimal(8),9,5);
        Vendedor v2 = new Vendedor("dni","80632213","Lopez","Joaquin Marcos","SantaRosa",new BigDecimal(100),20,9);
        
        EncargadoCom ec = new EncargadoCom("dni", "25177544", "Favore", "Veronica Claudia", "Casa93", new BigDecimal(50000));
        ec.getVendedores().add(v1);
        ec.getVendedores().add(v2);
        
        
        assertEquals(ec.calcular_sueldo(), new BigDecimal(54150).setScale(2, RoundingMode.HALF_EVEN));
    }

}