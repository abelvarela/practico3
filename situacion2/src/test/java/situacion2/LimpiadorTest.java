package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.math.RoundingMode;



public class LimpiadorTest {
    @Test
    public void calcularSueldoSinHorasExtra(){
        Limpiador limpiador = new Limpiador("dni","37632213","Varela","Abel Alejandro","Polcos",new BigDecimal(100),0);

        assertEquals(limpiador.calcular_sueldo(), new BigDecimal(100).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void calcularSueldoConHorasExtra(){
        Limpiador limpiador = new Limpiador("dni","37632213","Varela","Abel Alejandro","Polcos",new BigDecimal(100),10);

        assertEquals(limpiador.calcular_sueldo(), new BigDecimal(105).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void calcularSueldoSinHorasExtraMayorA55(){
        Limpiador limpiador = new Limpiador("dni","37632213","Varela","Abel Alejandro","Polcos",new BigDecimal(100),0);

        assertEquals(limpiador.calcular_sueldo(), new BigDecimal(100).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void calcularSueldoConHorasExtraMayorA55(){
        Limpiador limpiador = new Limpiador("dni","37632213","Varela","Abel Alejandro","Polcos",new BigDecimal(55000),10);

        assertEquals(limpiador.calcular_sueldo(), new BigDecimal(54862.50).setScale(2, RoundingMode.HALF_EVEN));
    }
}