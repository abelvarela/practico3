package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.math.RoundingMode;



public class PersonalAdmTest {

    @Test
    public void calcularSueldoMayorA55(){
        PersonalAdm personaladm = new PersonalAdm("dni","80632213","Lopez","Joaquin Marcos","SantaRosa",new BigDecimal(56000));

        assertEquals(personaladm.calcular_sueldo(), new BigDecimal(53200).setScale(2, RoundingMode.HALF_EVEN));
    }


}