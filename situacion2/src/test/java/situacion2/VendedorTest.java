package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.math.RoundingMode;



public class VendedorTest {
    @Test
    public void calcularSueldoContratosMenoresaDiez(){
        Vendedor vendedor = new Vendedor("dni","80632213","Lopez","Joaquin Marcos","SantaRosa",new BigDecimal(100),7,5);
        
        assertEquals(vendedor.calcular_sueldo(), new BigDecimal(107).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void calcularSueldoContratosMayorIgualaDiez(){
        Vendedor vendedor = new Vendedor("dni","80632213","Lopez","Joaquin Marcos","SantaRosa",new BigDecimal(100),20,5);

        assertEquals(vendedor.calcular_sueldo(), new BigDecimal(130).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void calcularSueldoContratosMenoresaDiezMayorA55(){
        Vendedor vendedor = new Vendedor("dni","80632213","Lopez","Joaquin Marcos","SantaRosa",new BigDecimal(55000),7,5);
        
        assertEquals(vendedor.calcular_sueldo(), new BigDecimal(55907.50).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void calcularSueldoContratosMayorIgualaDiezMayorA55(){
        Vendedor vendedor = new Vendedor("dni","80632213","Lopez","Joaquin Marcos","SantaRosa",new BigDecimal(55000),20,5);

        assertEquals(vendedor.calcular_sueldo(), new BigDecimal(67925).setScale(2, RoundingMode.HALF_EVEN));
    }


}